# TP2 : On va router des trucs

## 1. Echange ARP

### 🌞 Générer des requêtes ARP
- Effectuer un ping d'une machine à l'autre
```
[glama@node1 ~]$ ping 10.2.1.12
PING 10.2.1.12 (10.2.1.12) 56(84) bytes of data.
64 bytes from 10.2.1.12: icmp_seq=1 ttl=64 time=0.985 ms
64 bytes from 10.2.1.12: icmp_seq=2 ttl=64 time=0.487 ms
```
```
[glama@node2 ~]$ ping 10.2.1.11
PING 10.2.1.11 (10.2.1.11) 56(84) bytes of data.
64 bytes from 10.2.1.11: icmp_seq=1 ttl=64 time=0.491 ms
64 bytes from 10.2.1.11: icmp_seq=2 ttl=64 time=0.479 ms
```
- Observer les tables ARP des deux machines
```
[glama@node1 ~]$ ip n s
10.2.1.1 dev enp0s8 lladdr 0a:00:27:00:00:18 DELAY
10.2.1.12 dev enp0s8 lladdr 08:00:27:24:84:d4 STALE
```

```
[glama@node2 ~]$ ip n s
10.2.1.1 dev enp0s8 lladdr 0a:00:27:00:00:18 REACHABLE
10.2.1.11 dev enp0s8 lladdr 08:00:27:bb:31:73 STALE
```
- Repérer l'adresse MAC de node1 dans la table ARP de node2 et vice-versa

MAC node2 ``` 08:00:27:24:84:d4 ```
MAC node1 ``` 08:00:27:bb:31:73 ```


- Prouvez que l'info est correcte

  - une commande pour voir la table ARP de node1
    ```
      [glama@node1 ~]$ arp -a
    ? (10.2.1.1) at 0a:00:27:00:00:18 [ether] on enp0s8
    ? (10.2.1.12) at 08:00:27:24:84:d4 [ether] on enp0s8
    ``` 
  - et une commande pour voir la MAC de node2
    ```
    [glama@node2 ~]$ ip a
    [...]
    2: enp0s8: [...]
    link/ether 08:00:27:24:84:d4 
    [...]
    ```
    
## 2. Analyse de trames

### 🌞 Analyse de trames

- Utilisez la commande tcpdump pour réaliser une capture de trame
```
[glama@node1 ~]$ sudo tcpdump -i enp0s8 -w tp2_arp.pcap
dropped privs to tcpdump
tcpdump: listening on enp0s8, link-type EN10MB (Ethernet), capture size 262144 bytes
```
- Videz vos tables ARP, sur les deux machines, puis effectuez un ping
```
[glama@node1 ~]$ sudo ip neigh flush all
```
```
[glama@node2 ~]$ sudo ip neigh flush all
```

```
[glama@node2 ~]$ ping 10.2.1.11
PING 10.2.1.11 (10.2.1.11) 56(84) bytes of data.
64 bytes from 10.2.1.11: icmp_seq=1 ttl=64 time=1.09 ms
64 bytes from 10.2.1.11: icmp_seq=2 ttl=64 time=0.496 ms
```
- Stoppez la capture, et exportez-la sur votre hôte pour visualiser avec Wireshark
```
21 packets captured
22 packets received by filter
0 packets dropped by kernel
```
![](img/HzN7UqIt8d.png)

- Mettez en évidence les trames ARP

![](img/TNx3PxVQ5q.png)

- Ecrivez, dans l'ordre, les échanges ARP qui ont eu lieu, je veux TOUTES les trames


| ordre | type trame | source | destination |
| -------- | -------- | -------- | -------- | 
| 1    | Requête ARP     | node2 08:00:27:24:84:d4 | Broadcast FF:FF:FF:FF:FF:FF     |
| 2    | Réponse ARP     | node1 08:00:27:bb:31:73 | node2 08:00:27:24:84:d4 |
| 3    | Requête ARP     | node1 08:00:27:bb:31:73 | Broadcast FF:FF:FF:FF:FF:FF     |
| 4    | Réponse ARP     | node2 08:00:27:24:84:d4 | node1 08:00:27:bb:31:73 |

## II. Routage

### 1. Mise en place du routage

- 🌞Activer le routage sur le noeud router.net2.tp2
```
[glama@router ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8 enp0s9
  sources:
  services: cockpit dhcpv6-client ssh
  ports:
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
[glama@router ~]$ sudo firewall-cmd --get-active-zone
public
  interfaces: enp0s3 enp0s8 enp0s9
[glama@router ~]$ sudo firewall-cmd --add-masquerade --zone=public
success
[glama@router ~]$ sudo firewall-cmd --add-masquerade --zone=public --permanent
success
```
- 🌞Ajouter les routes statiques nécessaires pour que node1.net1.tp2 et marcel.net2.tp2 puissent se ping

Config de la route de la node1
```
[glama@node1 ~]$ cat /etc/sysconfig/network-scripts/route-enp0s8
10.2.2.0/24 via 10.2.1.254 dev enp0s8
```
Config de la route de marcel
```
[glama@marcel ~]$ cat /etc/sysconfig/network-scripts/route-enp0s8
10.2.1.0/24 via 10.2.2.254 dev enp0s8
```
Config de la carte réseau marcel
```
[glama@marcel ~]$ cat /etc/sysconfig/network-scripts/ifcfg-enp0s8
[...]
GATEWAY=10.2.2.254
```
Config de la carte réseau de node1
```
[glama@node1 ~]$ cat /etc/sysconfig/network-scripts/ifcfg-enp0s8
[...]
GATEWAY=10.2.1.254
``` 
Ping node1 vers marcel
```
[glama@node1 ~]$ ping 10.2.2.12
PING 10.2.2.12 (10.2.2.12) 56(84) bytes of data.
64 bytes from 10.2.2.12: icmp_seq=1 ttl=63 time=1.03 ms
64 bytes from 10.2.2.12: icmp_seq=2 ttl=63 time=0.918 ms
```
Ping marcel vers node1
```
[glama@marcel ~]$ ping 10.2.1.11
PING 10.2.1.11 (10.2.1.11) 56(84) bytes of data.
64 bytes from 10.2.1.11: icmp_seq=1 ttl=63 time=0.943 ms
64 bytes from 10.2.1.11: icmp_seq=2 ttl=63 time=1.37 ms
```

### 2. Analyse de trames

- 🌞Analyse des échanges ARP
    - videz les tables ARP des trois noeuds
    ```
    [glama@router network-scripts]$ sudo ip neigh flush all
    ```
    - effectuez un ping de node1.net1.tp2 vers marcel.net2.tp2
    ```
    [glama@node1 ~]$ ping 10.2.2.12
    PING 10.2.2.12 (10.2.2.12) 56(84) bytes of data.
    64 bytes from 10.2.2.12: icmp_seq=1 ttl=63 time=1.83 ms
    64 bytes from 10.2.2.12: icmp_seq=2 ttl=63 time=0.992 ms
    ```
    - regardez les tables ARP des trois noeuds
    ```
    [glama@node1 ~]$ ip neigh show
    10.2.1.1 dev enp0s8 lladdr 0a:00:27:00:00:18 DELAY
    10.2.1.254 dev enp0s8 lladdr 08:00:27:d6:df:70 STALE
    ```
    ```
    [glama@marcel ~]$ ip neigh show
    10.2.2.1 dev enp0s8 lladdr 0a:00:27:00:00:06 DELAY
    10.2.2.254 dev enp0s8 lladdr 08:00:27:0d:ff:73 STALE
    ```
    ```
    [glama@router network-scripts]$ ip neigh show
    10.2.2.12 dev enp0s9 lladdr 08:00:27:66:a4:e6 STALE
    10.2.1.11 dev enp0s8 lladdr 08:00:27:bb:31:73 STALE
    10.2.1.1 dev enp0s8 lladdr 0a:00:27:00:00:18 DELAY
    ```
    - essayez de déduire un peu les échanges ARP qui ont eu lieu
    ```Node 1 n'a pas obtenu l'adresse mac de Marcel et l'inverse aussi, en revanche ils ont la MAC de la passerelle donc de la carte réseau du routeur, le routeur a quand a lui la mac des 2 machine. Etant donné qu'on change de réseau il est impossible de récupérer la mac de l'autre machine dans sa table ARP.```
    - répétez l'opération précédente (vider les tables, puis ping), en lançant tcpdump sur node1 et marcel, afin de capturer les échanges depuis les 2 points de vue
        ```
        [glama@router network-scripts]$ sudo ip neigh flush all
        ```
        ```
        [glama@node1 ~]$ sudo tcpdump -i enp0s8 -w node1.pcap
        dropped privs to tcpdump
        tcpdump: listening on enp0s8, link-type EN10MB (Ethernet), capture size 262144 bytes
        ^C20 packets captured
        21 packets received by filter
        0 packets dropped by kernel
        ```
    - écrivez, dans l'ordre, les échanges ARP qui ont eu lieu, puis le ping et le pong, je veux TOUTES les trames utiles pour l'échange
```
| ordre | type trame  | IP source | MAC source                | IP destination | MAC destination            |
|-------|-------------|-----------|---------------------------|----------------|----------------------------|
| 1     | Requête ARP | x         | `node1` `08:00:27:bb:31:73| x              | Broadcast `FF:FF:FF:FF:FF` |
| 2     | Réponse ARP | x         | `marcel` `08:00:27:bb:31:73`| x            | `node1` `08:00:27:bb:31:73`|
| ...   | ...         | ...       | ...                       |                |                            |
| 3     | Ping        | 10.2.1.11 | 08:00:27:bb:31:73         | 10.2.2.12      | 08:00:27:d6:df:70          |
| 4     | Pong        | 10.2.2.12 | 08:00:27:d6:df:70         | 10.2.1.11      | 08:00:27:bb:31:73          |

```
### 3. Accès internet

#### 🌞Donnez un accès internet à vos machines
 - vérifiez que vous avez accès internet avec un ping
 ```
 [glama@node1 ~]$ ping 8.8.8.8
 PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
 64 bytes from 8.8.8.8: icmp_seq=1 ttl=111 time=75.4 ms
 64 bytes from 8.8.8.8: icmp_seq=2 ttl=111 time=45.1 ms
 ```
 - donnez leur aussi l'adresse d'un serveur DNS qu'ils peuvent utiliser
 ```
 [glama@node1 ~]$ cat /etc/sysconfig/network-scripts/ifcfg-enp0s8
[...]
DNS1=1.1.1.1

[glama@node1 ~]$ sudo nmcli con reload
[glama@node1 ~]$ sudo nmcli con up enp0s8
Connection successfully activated (D-Bus active path: /org/freedesktop/NetworkManager/ActiveConnection/3)
```
- vérifiez que vous avez une résolution de noms qui fonctionne avec dig
```
[glama@node1 ~]$ dig ynov.com

[...]
;; Query time: 71 msec
;; SERVER: 1.1.1.1#53(1.1.1.1)
[...]
```
- puis avec un ping vers un nom de domaine
```
[glama@node1 ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=111 time=52.3 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=111 time=43.9 ms
```

#### 🌞Analyse de trames

- effectuez un ping 8.8.8.8 depuis node1.net1.tp2
```
[glama@node1 ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=111 time=52.3 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=111 time=43.9 ms
```

- analysez un ping aller et le retour qui correspond et mettez dans un tableau :

| ordre | type trame | IP source           | MAC source               | IP destination | MAC destination |     |
|-------|------------|---------------------|--------------------------|----------------|-----------------|-----|
| 1     | ping       | `node1` `10.2.1.12` | `node1` `08:00:27:bb:31:73` | `8.8.8.8`   | `08:00:27:d6:df:70`|     |
| 2     | pong       | `8.8.8.8`           | `08:00:27:d6:df:70`      | `10.2.1.11`            | `08:00:27:bb:31:73`| ... |
